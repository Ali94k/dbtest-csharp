﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBTest
{
    class ConfigExample
    {
        public const string FILE_PATH = @"E:\debts_data_500000.csv";
        public const string PSQL_CONN_STR = "Server=IP;Port=5432;User Id=user;Password=pass;Database=dbname;";
        public const string MSSQL_CONN_STR = "Data Source=ip;Initial Catalog=dbname;Persist Security Info=True;Integrated Security=true;Max Pool Size=500";
        public const string INSERT_DEBTS = @"insert into DEBTS(""DEBTID"", ...) values(
        @0, @1, @2, @3, @4, @5, @6, @7, @8, @9, @10, @11, @12, @13, @14, @15, @16, @17, @18, @19, @20)";
    }
}

﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBTest
{
    class DBHelper
    {
        public DbConnection _dbConnection;
        private DbTransaction _transaction;
        public DbCommand command;

        public DBHelper(string dbProvider)
        {
            _dbConnection = getDbConnection(dbProvider);
        }

        public void Open()
        {
            
            _dbConnection.Open();

            //_transaction = _dbConnection.BeginTransaction();
            command = _dbConnection.CreateCommand();
            //command.Transaction = _transaction;
        }

        public void Close()
        {
            _transaction?.Dispose();
            _dbConnection?.Close();
            _dbConnection?.Dispose();
        }

        private DbConnection getDbConnection(string dbProvider)
        {
            switch (dbProvider)
            {
                case "psql":
                    return getPostgresqlConnection();
                case "mssql":
                    return getSqlServerConnection();
                default:
                    throw new Exception("Can't find provider name = " + dbProvider);
            }
        }

        private DbConnection getPostgresqlConnection()
        {
            return new NpgsqlConnection(Config.PSQL_CONN_STR);
        }
        private DbConnection getSqlServerConnection()
        {
            return new SqlConnection(Config.MSSQL_CONN_STR);
        }
    }
}
